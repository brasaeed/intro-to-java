package com.sda.firstlecture;


import java.util.Date;

public class Application {

	static int age = 10;

	static String name = "Saeed";

	static float money = 10.2F;
	static char c;


	public static void main(String[] args) {
		System.out.println(args[0]);
		/*
		double money = 10.22F;
		int year = 2019;
		System.out.print(age);
		System.out.println();
		System.out.println();
		System.out.printf("%X:%X:%X:%X",192,168,1,10);*/

		/**
		 * Accept input from user.
		 * Check if input is 4 and then print yeaaaaah!
		 */
		/*Scanner input = new Scanner(System.in);
		int number = Integer.parseInt(input.nextLine());
		if (number == 4) {
			System.out.println("yeaaaaah!");
		} else if (number == 5) {
			System.out.println("yeaaaaah! number is 5");
		} else {
			System.out.println("Number is " + number);
		}

		switch (number) {
			case 4:
				System.out.println("yeaaaaah! from switch");
				break;
			case 5:
				System.out.println("yeaaaaah! number is 5 from switch");
				break;
			default:
				System.out.println("Number is from switch " + number);
		}*/


		//loops
		/*boolean a = true;
		while (a) {
			System.out.println("a == true");
			a = false;
		}

		for (int num = 5; num < 11; num++) {
			System.out.println(num);
		}
		int num = 4;
		do {
			System.out.println(num);
			num--;
		} while (num > 1);

		String aName = "Mario";
		for (int i = 0; i < aName.length(); i++) {

			System.out.println(aName.charAt(i));
			System.out.println("After break");
		}
*/
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print("*");
			}
			System.out.println();
		}

		//arrays

		int[] intArrays = {1, 2, 3, 4, 5, 6};

		for (int number : intArrays) {
			System.out.println(number);
		}

		int[] newArrays = new int[5];
		intArrays[0] = 4;
		System.out.println(intArrays[0]);

		//OOP

		Animal cat = new Animal("Cat", "Inhale", "Eat");

		cat.printType();
		System.out.println(Animal.name);
		cat.setEat();


		Animal dog = new Animal("Dog", "Inhale 1", "Eat 1");

		dog.printType();
		dog.setEat();

		System.out.println(new Date().getTime());
		System.out.println(System.currentTimeMillis());

	}

}

