package com.sda.firstlecture;

public class Animal {
	private String breathe;
	String eat;
	String type;


	static String name = "Simon";


	public Animal(String type, String breatheA, String eatA){
		this.type = type;
		eat = eatA;
		breathe = breatheA;
		System.out.println("Constructor");
	}

	private void setBreathe(){
		System.out.println(breathe);
	}
	public void setEat(){
		System.out.println(eat);
	}
	public void printType(){
		System.out.println(type);
	}

}
